from PIL import Image, ImageEnhance, ImageFilter, ImageOps
import sys

def process_image(image_dir):
    img = Image.open(image_dir)
    img = ImageEnhance.Color(img).enhance(0.0)          # Greyscale
    img = ImageEnhance.Brightness(img).enhance(0.1)     # Darkens image so only chars stand out
    img = ImageEnhance.Sharpness(img).enhance(2.0)      # Sharpens lines to reduce glow effect
    img = img.filter(ImageFilter.MedianFilter(size=3))  # Further reduces glow effect
    img = ImageEnhance.Contrast(img).enhance(10.0)      # Greatly increases contrast leaving only charecters
    img = ImageOps.invert(img)                          # Inverts image to be black text on white background
    return img

def save_image(img, name):
    save_dir = 'samples/' + name
    img.save(save_dir)

#Temp for bash testing
#i = process_image(sys.argv[1])
#save_image(i, sys.argv[2])