# RLStatReader

This serverless application captures data from esports scoreboard screenshots for later storage and analytics by esports leagues and other grassroots programs.

With the decreased cost of serverless platforms, most growing, small scale esports league should be able to bootstrap their own analytics platform for all their games for nearly free, all without reliance on finicky or potentially nonexistant / nonavailable APIs.

This app is currently being designed for Rekognition, and tested with Rocket League scoreboards.

## Getting Started

For now, download the repo and run the following to prepare some sample images for consumption by Rekognition:

```python3 preprocess.py samples/crop3.jpg test.jpg```

More coming soon!