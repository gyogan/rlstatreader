import boto3

def call_rekognition(imageFile):
    client = boto3.client('rekognition')

    with open(imageFile, 'rb') as image:
        response = client.detect_text(Image={
            'Bytes': image.read()
        })

        return response
