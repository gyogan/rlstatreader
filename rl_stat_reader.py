import sys
import time
import datetime

import preprocess
import crop_image
import ocr_parse

def main():
  print('Using file: ' + sys.argv[1])
  img = preprocess.process_image(sys.argv[1])

  st = datetime.datetime.fromtimestamp(time.time()).strftime('%Y%m%d-%H:%M:%S')
  filename = 'preprocess_' + st + '.jpg'
  print('Preprocessed image saved as: ' + st)
  preprocess.save_image(img, filename)
  response = ocr_parse.call_rekognition('samples/' + filename)

  print(response)

if __name__== "__main__":
  main()